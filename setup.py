from setuptools import setup

setup(name='snapskiff_helloworld',
      version='0.0.1',
      packages=['snapskiff_helloworld'],
      entry_points={
          'console_scripts': [
              'snapskiff_helloworld = snapskiff_helloworld.__main__:main'
          ]
      },
      )
