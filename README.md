# SnapSkiff Client

SnapSkiff is the simplest possible alternative to the centralized Snap Store. The centralized store is replaced with another, more different centralized store.

This repository contains the `snapskiff-helloworld` snap, which is just a strictly confined snap that says "Hello, world!" when run.
